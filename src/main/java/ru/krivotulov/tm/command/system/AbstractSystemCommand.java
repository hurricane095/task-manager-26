package ru.krivotulov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.service.ICommandService;
import ru.krivotulov.tm.api.service.IPropertyService;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.enumerated.Role;

/**
 * AbstractSystemCommand
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    public ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }
}
