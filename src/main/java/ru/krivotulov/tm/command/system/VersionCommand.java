package ru.krivotulov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.command.AbstractCommand;

/**
 * VersionCommand
 *
 * @author Aleksey_Krivotulov
 */
public class VersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String DESCRIPTION = "Display application version.";

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println(getPropertyService().getApplicationVersion());
    }

}
