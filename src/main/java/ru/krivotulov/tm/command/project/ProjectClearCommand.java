package ru.krivotulov.tm.command.project;

import org.jetbrains.annotations.NotNull;

/**
 * ProjectClearCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        @NotNull final String userId = getUserId();
        getProjectService().clear(userId);
    }

}
