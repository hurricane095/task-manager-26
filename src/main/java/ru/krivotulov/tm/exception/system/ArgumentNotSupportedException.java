package ru.krivotulov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! argument not supported...");
    }

    public ArgumentNotSupportedException(@NotNull final String argument) {
        super("Error! argument ``" + argument + "`` not supported...");
    }

}
