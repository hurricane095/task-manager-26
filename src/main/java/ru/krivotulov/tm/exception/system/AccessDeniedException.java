package ru.krivotulov.tm.exception.system;

import ru.krivotulov.tm.exception.AbstractException;

/**
 * AccessDeniedException
 *
 * @author Aleksey_Krivotulov
 */
public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
