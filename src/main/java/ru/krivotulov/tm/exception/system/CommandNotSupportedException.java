package ru.krivotulov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.exception.AbstractException;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! command not supported...");
    }

    public CommandNotSupportedException(@NotNull final String command) {
        super("Error! command ``" + command + "`` not supported...");
    }

}
