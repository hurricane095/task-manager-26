package ru.krivotulov.tm.exception.field;

public final class NameEmptyException extends AbstractFieldException {

    public NameEmptyException() {
        super("Error! name is empty...");
    }

}
