package ru.krivotulov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(@NotNull final String userId,
                          @NotNull final String name) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId,
                          @NotNull final String name,
                          @NotNull final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

}
