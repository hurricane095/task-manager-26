package ru.krivotulov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.IUserOwnedRepository;
import ru.krivotulov.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * AbstractUserRepository
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId,
                 @NotNull final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return models.stream()
                .filter(model -> userId.equals(model.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId,
                           @NotNull final Comparator<M> comparator
    ) {
        return models.stream()
                .filter(model -> userId.equals(model.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId,
                         @NotNull final String id
    ) {
        return models.stream()
                .filter(model -> id.equals(model.getId()) && userId.equals(model.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId,
                            @NotNull final Integer index
    ) {
        if(index > models.size()) return null;
        return models.get(index);
    }

    @Nullable
    @Override
    public M delete(@NotNull final String userId,
                    @NotNull final M model
    ) {
        return deleteById(userId, model.getUserId());
    }

    @Nullable
    @Override
    public M deleteById(@NotNull final String userId,
                        @NotNull final String id) {
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return delete(model);
    }

    @Nullable
    @Override
    public M deleteByIndex(@NotNull final String userId,
                           @NotNull final Integer index
    ) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return delete(model);
    }

    @Override
    public boolean existsById(@NotNull final String userId,
                              @NotNull final String id
    ) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return findAll(userId).size();
    }

}
