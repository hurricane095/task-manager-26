package ru.krivotulov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.IRepository;
import ru.krivotulov.tm.model.AbstractModel;
import ru.krivotulov.tm.model.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * AbstractRepository
 *
 * @author Aleksey_Krivotulov
 */
public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected List<M> models = new ArrayList<>();

    @NotNull
    protected Predicate<M> filterById(@NotNull final String id){
        return model -> id.equals(model.getId());
    }

    @Override
    public void clear() {
        models.clear();
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
      return models.stream()
              .sorted(comparator)
              .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return models
                .stream()
                .filter(filterById(id))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        if (index > models.size() || models.isEmpty()) return null;
        return models.get(index);
    }

    @NotNull
    @Override
    public M delete(@NotNull final M model) {
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M deleteById(@NotNull final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M deleteByIndex(@NotNull final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
        models.removeAll(collection);
    }

}
