package ru.krivotulov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.api.service.ITaskService;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.exception.entity.TaskNotFoundException;
import ru.krivotulov.tm.exception.field.*;
import ru.krivotulov.tm.model.Task;

import java.util.*;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final ITaskRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId,
                       @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId,
                       @Nullable final String name,
                       @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId,
                       @Nullable final String name,
                       @Nullable final String description,
                       @Nullable final Date dateBegin,
                       @Nullable final Date dateEnd
    ) {
        return Optional.of(create(userId, name, description))
                .map(task -> {
                    task.setDateBegin(dateBegin);
                    task.setDateEnd(dateEnd);
                    return task;
                }).orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId,
                                         @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, taskId);
    }

    @NotNull
    @Override
    public Task updateById(@Nullable final String userId,
                           @Nullable final String id,
                           @Nullable final String name,
                           @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if(description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return Optional.ofNullable(findOneById(userId, id))
                .map(task -> {
                    task.setName(name);
                    task.setDescription(description);
                    return task;
                }).orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task updateByIndex(@Nullable final String userId,
                              @Nullable final Integer index,
                              @Nullable final String name,
                              @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if(description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return Optional.ofNullable(findOneByIndex(userId, index))
                .map(task -> {
                    task.setName(name);
                    task.setDescription(description);
                    return task;
                }).orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task changeStatusById(@Nullable final String userId,
                                 @Nullable final String id,
                                 @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if(status == null || !Arrays.asList(Status.values()).contains(status)) throw new StatusIncorrectException();
        return Optional.ofNullable(findOneById(userId, id))
                .map(task -> {
                    task.setStatus(status);
                    return task;
                }).orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task changeStatusByIndex(@Nullable final String userId,
                                    @Nullable final Integer index,
                                    @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if(status == null || !Arrays.asList(Status.values()).contains(status)) throw new StatusIncorrectException();
        return Optional.ofNullable(findOneByIndex(userId, index))
                .map(task -> {
                    task.setStatus(status);
                    return task;
                }).orElseThrow(TaskNotFoundException::new);
    }

}
