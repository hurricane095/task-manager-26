package ru.krivotulov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.IRepository;
import ru.krivotulov.tm.api.service.IService;
import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.exception.entity.ModelNotFoundException;
import ru.krivotulov.tm.exception.field.IdEmptyException;
import ru.krivotulov.tm.exception.field.IndexIncorrectException;
import ru.krivotulov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * AbstractService
 *
 * @author Aleksey_Krivotulov
 */
public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Nullable
    @Override
    public M delete(@Nullable final M model) {
        if (model == null) return null;
        return repository.delete(model);
    }

    @NotNull
    @Override
    public M deleteById(@Nullable final  String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.deleteById(id);
    }

    @NotNull
    @Override
    public M deleteByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.deleteByIndex(index);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public void removeAll(@Nullable Collection<M> collection) {
        if(collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

}
