package ru.krivotulov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.api.component.ISaltProvider;

/**
 * IPropertyService
 *
 * @author Aleksey_Krivotulov
 */
public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
