package ru.krivotulov.tm.api.service;

import org.jetbrains.annotations.NotNull;

/**
 * IServiceLocator
 *
 * @author Aleksey_Krivotulov
 */
public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

}
