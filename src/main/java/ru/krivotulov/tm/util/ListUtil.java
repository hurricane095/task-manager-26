package ru.krivotulov.tm.util;

import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.model.AbstractModel;

import java.util.Collection;

/**
 * ListUtil
 *
 * @author Aleksey_Krivotulov
 */
public interface ListUtil {

    @Nullable
    static <T> Collection<T> emptyListToNull(@Nullable Collection<T> collection) {
        return collection == null || collection.isEmpty() ? null : collection;
    }

}
